package pl.edu.pjwstk.cache;

public class Main {
    public static void main(String[] args) {
        Cache instance = Cache.getInstance();

        try {
            instance.put(new CacheEntry(1, "GA", "City", "Gdynia"));
            instance.put(new CacheEntry(2, "GSP", "City", "Sopot"));
            instance.put(new CacheEntry(3, "GD", "City", "Gdańsk"));

            System.out.println(instance.get("GA").getValue());
            System.out.println(instance.get("City", 2).getValue());
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
