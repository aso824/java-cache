package pl.edu.pjwstk.cache.Implementations.Storages;

import pl.edu.pjwstk.cache.CacheEntry;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.BrokenFileException;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.FileInaccesibleException;
import pl.edu.pjwstk.cache.Interfaces.IFileDatabase;

import java.util.LinkedList;
import java.util.List;

public class CsvStorage implements IFileDatabase {
    @Override
    public void open(String filename) throws FileInaccesibleException {
        //
    }

    @Override
    public void close() {
        //
    }

    @Override
    public List<CacheEntry> loadData() throws BrokenFileException {
        return new LinkedList<CacheEntry>();
    }

    @Override
    public void updateData(List<CacheEntry> entries) throws FileInaccesibleException {
        //
    }
}
