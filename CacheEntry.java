package pl.edu.pjwstk.cache;

public class CacheEntry {
    private int intKey;
    private String stringKey;

    private String enumerationName;
    private String value;

    public CacheEntry(int intKey, String stringKey, String enumerationName, String value) {
        this.intKey = intKey;
        this.stringKey = stringKey;
        this.enumerationName = enumerationName;
        this.value = value;
    }

    public int getIntKey() {
        return intKey;
    }

    public void setIntKey(int intKey) {
        this.intKey = intKey;
    }

    public String getStringKey() {
        return stringKey;
    }

    public void setStringKey(String stringKey) {
        this.stringKey = stringKey;
    }

    public String getEnumerationName() {
        return enumerationName;
    }

    public void setEnumerationName(String enumerationName) {
        this.enumerationName = enumerationName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
