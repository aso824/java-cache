package pl.edu.pjwstk.cache.Exceptions;

public class StorageException extends Exception {
    public StorageException(String message) {
        super(message);
    }
}
