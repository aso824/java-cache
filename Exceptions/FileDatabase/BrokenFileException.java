package pl.edu.pjwstk.cache.Exceptions.FileDatabase;

public class BrokenFileException extends Exception {
    BrokenFileException(String message) {
        super(message);
    }
}
