package pl.edu.pjwstk.cache.Implementations;

import pl.edu.pjwstk.cache.CacheEntry;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.BrokenFileException;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.FileInaccesibleException;
import pl.edu.pjwstk.cache.Exceptions.StorageException;
import pl.edu.pjwstk.cache.Implementations.Storages.CsvStorage;
import pl.edu.pjwstk.cache.Interfaces.ICacheDriver;

import java.util.List;

public class FileCacheDriver implements ICacheDriver {
    private CsvStorage storage;

    public FileCacheDriver() throws StorageException {
        storage = new CsvStorage();

        try {
            storage.open("cache.csv");
        } catch (FileInaccesibleException e) {
            throw new StorageException(e.getMessage());
        }
    }

    @Override
    public void save(List<CacheEntry> data) throws FileInaccesibleException {
        storage.updateData(data);
    }

    @Override
    public List<CacheEntry> load() throws BrokenFileException {
        return storage.loadData();
    }
}
