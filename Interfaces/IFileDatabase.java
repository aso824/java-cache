package pl.edu.pjwstk.cache.Interfaces;

import pl.edu.pjwstk.cache.CacheEntry;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.BrokenFileException;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.FileInaccesibleException;

import java.util.List;

public interface IFileDatabase {
    void open(String filename) throws FileInaccesibleException;
    void close();

    List<CacheEntry> loadData() throws BrokenFileException;
    void updateData(List<CacheEntry> entries) throws FileInaccesibleException;
}
