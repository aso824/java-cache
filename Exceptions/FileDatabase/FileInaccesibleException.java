package pl.edu.pjwstk.cache.Exceptions.FileDatabase;

public class FileInaccesibleException extends Exception {
    FileInaccesibleException(String message) {
        super(message);
    }
}
