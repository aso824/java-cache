package pl.edu.pjwstk.cache.Interfaces;

import pl.edu.pjwstk.cache.CacheEntry;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.BrokenFileException;
import pl.edu.pjwstk.cache.Exceptions.FileDatabase.FileInaccesibleException;

import java.util.List;

public interface ICacheDriver {
    void save(List<CacheEntry> data) throws FileInaccesibleException;
    List<CacheEntry> load() throws BrokenFileException;
}
