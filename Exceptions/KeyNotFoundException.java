package pl.edu.pjwstk.cache.Exceptions;

public class KeyNotFoundException extends Exception {
    public KeyNotFoundException(String message) {
        super(message);
    }

    public KeyNotFoundException() {

    }
}
