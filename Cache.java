package pl.edu.pjwstk.cache;

import pl.edu.pjwstk.cache.Exceptions.FileDatabase.FileInaccesibleException;
import pl.edu.pjwstk.cache.Exceptions.KeyNotFoundException;
import pl.edu.pjwstk.cache.Exceptions.StorageException;
import pl.edu.pjwstk.cache.Implementations.FileCacheDriver;
import pl.edu.pjwstk.cache.Interfaces.ICacheDriver;

import java.util.LinkedList;
import java.util.List;

public class Cache {
    private static Cache instance;

    static {
        try {
            instance = new Cache();
        } catch (StorageException e) {
            System.out.println("Nie udało się otworzyć cache: " + e.getMessage());
        }
    }

    private ICacheDriver driver;
    private List<CacheEntry> data;

    public static Cache getInstance() {
        return instance;
    }

    private Cache() throws StorageException {
        driver = new FileCacheDriver();
        data = new LinkedList<>();
    }

    public void put(CacheEntry entry) throws StorageException {
        boolean found = false;

        for(CacheEntry it: data) {
            if (it.getStringKey().equals(entry.getStringKey())) {
                it = entry;
                found = true;

                break;
            }
        }

        if (! found) {
            data.add(entry);
        }

        try {
            driver.save(data);
        } catch (FileInaccesibleException e) {
            throw new StorageException(e.getMessage());
        }
    }

    public CacheEntry get(String enumerationName, int intKey) throws KeyNotFoundException {
        for (CacheEntry it: data) {
            if (it.getEnumerationName().equals(enumerationName) && it.getIntKey() == intKey) {
                return it;
            }
        }

        throw new KeyNotFoundException();
    }

    public CacheEntry get(String stringKey) throws KeyNotFoundException {
        for (CacheEntry it: data) {
            if (it.getStringKey().equals(stringKey)) {
                return it;
            }
        }

        throw new KeyNotFoundException();
    }

    public boolean forget(String enumerationName, int intKey) throws StorageException {
        for (CacheEntry it: data) {
            if (it.getEnumerationName().equals(enumerationName) && it.getIntKey() == intKey) {
                it = null;
                try {
                    driver.save(data);
                } catch (FileInaccesibleException e) {
                    throw new StorageException(e.getMessage());
                }

                return true;
            }
        }

        return false;
    }

    public boolean forget(String stringKey) throws StorageException {
        for (CacheEntry it: data) {
            if (it.getStringKey().equals(stringKey)) {
                it = null;
                try {
                    driver.save(data);
                } catch (FileInaccesibleException e) {
                    throw new StorageException(e.getMessage());
                }

                return true;
            }
        }

        return false;
    }
}
